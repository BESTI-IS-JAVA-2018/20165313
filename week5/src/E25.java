import javax.swing.*; 
import java.io.*;
public class E25 {
   public static void main(String args[]) {
      byte b[]=new byte[30];
      try{  FileInputStream input=new FileInputStream("Example10_18.java");
            ProgressMonitorInputStream in=
            new ProgressMonitorInputStream(null,"读取java文件",input);
            ProgressMonitor p=in.getProgressMonitor();  
            while(in.read(b)!=-1) {
               String s=new String(b);
               System.out.print(s);
               Thread.sleep(1000);
            }
       }
       catch(Exception e){}
   }
}
