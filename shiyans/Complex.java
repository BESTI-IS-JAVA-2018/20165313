public class Complex {
    public double x;
    public double y;
    public  Complex(double x,double y){
        this.x=x;
        this.y=y;
    }
    public double getx(Complex a){
        return a.x;
    }
    public double gety(Complex a){
        return a.y;
    }
    Complex Complexadd(Complex a){
        return new Complex(this.x+a.x,this.y+a.y);

    }
    Complex ComplexSub(Complex a){
        return new Complex(this.x-a.x,this.y-a.y);

    }
    Complex ComplexMul(Complex a){
        return new Complex(this.x*a.x-this.y*a.y,this.x*a.y+this.y*a.x);

    }
    Complex ComplexDiv(Complex a){
        return new Complex
                ((this.x*a.x+this.y*a.y)/(a.x*a.x+a.y*a.y),(this.y*a.x-this.x*a.y)/(a.x*a.x+a.y*a.y));

    }
    public void  inputComplex(Complex a){
        System.out.println(a.getx(a));
        System.out.println(a.gety(a));
    }
}