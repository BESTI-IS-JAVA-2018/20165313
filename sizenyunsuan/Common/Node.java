package Common;

public class Node {  
    private String  value; 
    private Node leftChild;  
    private Node rightChild;  
    private boolean hasBrackets;
    
    public boolean getHasBrackets() {
		return hasBrackets;
	}

	public void setHasBrackets(boolean hasBrackets) {
		this.hasBrackets = hasBrackets;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Node getLeftChild() {
		return leftChild;
	}

	public void setLeftChild(Node leftChild) {
		this.leftChild = leftChild;
	}

	public Node getRightChild() {
		return rightChild;
	}

	public void setRightChild(Node rightChild) {
		this.rightChild = rightChild;
	}

	public Node(String value) {  
        this.value = value;
        leftChild = null;
        rightChild = null;
    }  
	public Node(Node node) {  
        this.value = node.getValue();
        if(node.getLeftChild()!=null)
        	this.leftChild = new Node(node.getLeftChild());
        else 
        	this.leftChild=null;
        if(node.getRightChild()!=null)
        	this.rightChild = new Node(node.getRightChild());
        else
        	this.rightChild=null;
        this.hasBrackets = node.getHasBrackets();
    }  
      
    public void display() {  
        System.out.print(this.value + "\t");  
    }  
  
    @Override  
    public String toString() {  
    	if(value.equals("+")||value.equals("-")||value.equals("*")||value.equals("➗"))
    		if(this.hasBrackets)
    			return "("+this.leftChild+this.value+this.rightChild+")";
    		else
    			return this.leftChild+this.value+this.rightChild;
    	else
    		return value;  
    }
    
}  