﻿package Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Common.Node;

public class Test {
	public static void main(String[] arg) throws Exception{
		ExpressionBuilder eb = new ExpressionBuilder();
		FileWirter fw = new FileWirter();
		Scanner  sc = new Scanner(System.in);
		System.out.println("请输入要生成题目数量");
		int n = sc.nextInt();
		System.out.println("请输入题目中数值最大值");
		int size = sc.nextInt();
		while(size>200) {
			System.out.println("数值过大，请重新输入题目中数值最大值");
			size = sc.nextInt();
		}
		List<Node> list1 = eb.CreateExpressionTreeList(n, size);
		int i=1;
		List<Integer> correctAnswer = new ArrayList<Integer>();
		List<Integer> wrongAnswer = new ArrayList<Integer>();
		for(Node s:list1){
			System.out.println("第"+i+"题:  "+s);
			String yourAnswer = sc.next();
			String answer = eb.FinalAnswer(s);
			if(yourAnswer.equals(answer)) {
				System.out.println("回答正确");
				correctAnswer.add(i);
			}else {
				System.out.println("回答错误");
				System.out.println("正确答案:  "+answer);
				wrongAnswer.add(i);
			}
			fw.wirter("第"+i+"题:  "+s);
			fw.wirter2("第"+i+"题答案:  "+answer);
			i++;
		}
		
		String CorrectS = "Correct:"+correctAnswer.size()+"(";
		for(Integer s:correctAnswer) {
			CorrectS+=(s+",");
		}
		CorrectS+=")";
		String WrongS = "Wrong:"+wrongAnswer.size()+"(";
		for(Integer s:wrongAnswer) {
			WrongS+=(s+",");
		}
		WrongS+=")";
		System.out.println(CorrectS);
		System.out.println(WrongS);
		fw.wirter3(CorrectS);
		fw.wirter3(WrongS);
	}
}
