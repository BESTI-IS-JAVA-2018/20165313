import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;
import java.util.Scanner;
public class SizeYunsuan {

	/**
	 * @param args
	 */
	public static Random rand=new Random();
    public static class Qst
    {
        static int Operand(int Range)//产生操作数
        {
            int Opd=rand.nextInt(Range*2+1)-Range;
            return Opd;
        }
        public static char OperatorKind(char Operator)//生成运算符
        {
            int OperatorPossible=rand.nextInt(3);
            
            switch(OperatorPossible)
            {
                case 0:
                    Operator='+';
                    break;
                case 1:
                    Operator='-';
                    break;
                case 2:
                    Operator='*';
                    break;
               
                default:
                    System.out.print("Error!");
            }
            return Operator;
        }
        public static boolean IfRepeated(String str[],int Location)//判断是否重复
        {
            for(int i=0;i<Location;i++)
            {
                if(str[i].equals(str[Location]))
                    return true;
            }
            return false;
        }
        public static int Ans(int ans,int Operand1,int Operand2,char Operator)//生成答案
        {
            switch(Operator)
            {
                case '+':
                    ans=Operand1+Operand2;
                    break;
                case '-':
                    ans=Operand1-Operand2;
                    break;
                case '*':
                    ans=Operand1*Operand2;
                    break;
               
         
                default:
                    System.out.print("Error!");
            }
            return ans;
        }
        //生成一道运算题
        public static void CreateStr(int Range,char Operator,String str[],int i,int QstNum,int ans[])
        {       int answer = 0;
                Qst.OperatorKind(Operator);
                int Operand1=Qst.Operand(Range);
                int Operand2=Qst.Operand(Range);
                str[i]=Integer.toString(Operand1);
                str[i]+=Operator;
                str[i]+=Integer.toString(Operand2);
                str[i]+="=";
                while(IfRepeated(str,i))//判断是否重复
                {
                    Operand1=Qst.Operand(Range);
                    Operand2=Qst.Operand(Range);
                    str[i]=Integer.toString(Operand1);
                    str[i]+=Operator;
                    str[i]+=Integer.toString(Operand2);
                    str[i]+="=";
                }
                    ans[i]=Qst.Ans(answer,Operand1,Operand2,Operator);
        }
        public static void Display(String str[])//输出生成的运算题
        {
             
            for(int j=0;j<str.length;j++)
            {
                System.out.print(str[j]);
                if(j%4==3)
                {
                    System.out.println();
                }
                else
                {
                    System.out.print('\t');
                }
            }
        }
        public static void Writefile(String str[],String filePath){
        	
        	try {
        		 File file = new File(filePath);
                 PrintStream ps = new PrintStream(new FileOutputStream(file));
                 for(int j=1;j<str.length;j++){
                 ps.println(j+"、"+str[j]);// 往文件里写入字符串
                
                 }
				
			} catch (FileNotFoundException e) {
				// TODO: handle exception
				 e.printStackTrace();
			}
        }
        public static void Input(int Input[],int QstNum)//输入问题答案
        {
            Scanner sca=new Scanner(System.in);
            for(int j=0;j<QstNum;j++)
            {
                Input[j]=sca.nextInt();
            }
        }
        public static void OutAns(int ans[],int QstNum)//输出答案
        {
            for(int j=0;j<QstNum;j++)
            {
                System.out.print(ans[j]);
                if(j%4==3)
                {
                    System.out.println();
                }
                else
                {
                    System.out.print('\t');
                }
            }
        }
        public static void ConfirmAns(int ans[],int Input[],int QstNum,int count)
        {   int a[] = new int[QstNum];
            count=0;
            for(int i=0;i<QstNum;i++)
            {
                if(ans[i]==Input[i]){
                    count++;
                    a[i]=i;
                }else{
                	a[i]=-1;
                }
            }
            try {
            	File file = new File("D:\\Grade.txt");
                PrintStream ps = new PrintStream(new FileOutputStream(file));
                System.out.print("Correct:");
                ps.print("Correct:");
                System.out.print(count);
                ps.print(count);
                System.out.print("(");
                ps.print("(");
                for(int i=0;i<a.length;i++){
                	
                	if(a[i]!=-1){
                		
                		 System.out.print(i+1+",");
                		 ps.print(i+1+",");
                		}
                	}
              
            	System.out.println(")");
            	 ps.println(")");
                System.out.print("Wrong：");
                ps.print("Wrong：");
                System.out.print(QstNum-count);
                ps.print(QstNum-count);
                System.out.print("(");
                ps.print("(");
                for(int i=0;i<a.length;i++){
                	
                	if(a[i]==-1){
                		
                		 System.out.print(i+1+",");
                		 ps.print(i+1+",");
                		}
                	}
             
           	System.out.println(")");
            ps.println(")");
				
			} catch (FileNotFoundException e) {
				// TODO: handle exception
			}

        }
        
         
    }
     
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int Range,QstNum=0,count=0;
        
        char Operator = '+';
        Scanner sca=new Scanner(System.in);
        System.out.println("请输入生成题目的数量:");
        QstNum=sca.nextInt();
        System.out.println("请输入算数范围:");
        Range=sca.nextInt();
        
        String str[] = new String[QstNum];
        int ans[]=new int[QstNum];
        int Input[]=new int[QstNum];
        for( int i=0;i<QstNum;i++)
        {
            try
            {
                Qst.CreateStr(Range,Qst.OperatorKind(Operator),str,i,QstNum,ans);
            }
            catch(Exception e)
            {
                i--;
            };
        }
        Qst.Display(str);  
        String filePath;
        filePath="D:\\Exercises.txt";
		Qst.Writefile(str, filePath);
        System.out.println();
        System.out.println("输入答案：");
        Qst.Input(Input, QstNum);
        System.out.println("正确答案：");
        Qst.OutAns(ans,QstNum);
        System.out.println();
        Qst.ConfirmAns(ans,Input,QstNum,count);
	}

}
